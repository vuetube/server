FROM node:12.4.0-alpine AS builder

RUN apk add --no-cache --virtual .gyp python make g++

WORKDIR /build
COPY ["package.json", "package-lock.json", "./"]

RUN npm install
COPY . .

RUN npm run build && \
    rm -rf node_modules && \
    npm install

# End builder stage

FROM node:12.4.0-alpine

ENV NODE_ENV=production
WORKDIR /app

COPY --from=builder /build/dist ./dist
COPY --from=builder /build/package.json .
COPY --from=builder /build/node_modules ./node_modules
COPY --from=builder /build/.env .
COPY --from=builder /build/.env.development .
COPY --from=builder /build/.env.production .

EXPOSE 8000
CMD [ "npm", "start" ]