import { NextFunction, Request, Response } from "express";
import { UserRequest } from "../config/request/user.request";

const { response } = require("../shared/utils/responseApi");

const asyncHandler = require("../shared/middleware/async");
const Category = require("../models/Category");

// @desc    Get categories
// @route   GET /api/v1/categories
// @access  Private/Admin
exports.getCategories = asyncHandler(
  async (req: Request, res: any, next: NextFunction) => {
    res.status(200).json(res.advancedResults);
  }
);

// @desc    Get single category
// @route   GET /api/v1/categories/:id
// @access  Private/Admin
exports.getCategory = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const category = await Category.findById(req.params.id);

    if (!category) {
      return res
        .status(404)
        .json(
          response(false, `No category with that id of ${req.params.id}`, {})
        );
    }

    res.status(200).json(response(true, "OK", category, {}));
  }
);

// @desc    Create Category
// @route   POST /api/v1/categories/
// @access  Private/Admin
exports.createCategory = asyncHandler(
  async (req: UserRequest, res: Response, next: NextFunction) => {
    const category = await Category.create({
      ...req.body,
      userId: req.user.id,
    });

    return res.status(200).json(response(true, "OK", category));
  }
);

// @desc    Update category
// @route   PUT /api/v1/categories
// @access  Private/Admin
exports.updateCategory = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const category = await Category.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
      context: "query",
    });

    if (!category) {
      return res
        .status(404)
        .json(
          response(false, `No category with that id of ${req.params.id}`, {})
        );
    }

    return res.status(200).json(response(true, "Update success", category));
  }
);

// @desc    Delete Category
// @route   DELETE /api/v1/categories/:id
// @access  Private/Admin
exports.deleteCategory = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    let category = await Category.findById(req.params.id);

    if (!category) {
      return res
        .status(404)
        .json(
          response(false, `No category with that id of ${req.params.id}`, {})
        );
    }

    await category.remove();

    return res.status(200).json(response(true, "Delete success", {}));
  }
);
