import { NextFunction, Request, Response } from "express";
import { UserRequest } from "../config/request/user.request";

const asyncHandler = require("../shared/middleware/async");
const advancedResultsFunc = require("../shared/utils/advancedResultsFunc");
const { response } = require("../shared/utils/responseApi");

const Video = require("../models/Video");
const Subscription = require("../models/Subscription");
// @desc    Get all subscribers
// @route   GET /api/v1/subscriptions/subscribers
// @access  Private
exports.getSubscribers = asyncHandler(
  async (req: Request, res: any, next: NextFunction) => {
    return res.status(200).json(res.advancedResults);
  }
);

// @desc    Get all channels subscribed to
// @route   GET /api/v1/subscriptions/channels/:id
// @access  Private
exports.getChannels = asyncHandler(
  async (req: UserRequest, res: any, next: NextFunction) => {
    const { channelId } = req.params;
    const channel = await Subscription.find({
      $and: [{ channelId: channelId }, { subscriberId: req.user._id }],
    }).populate(["channelId", "videos"]);

    return res.status(200).json(response(true, "getChannels", channel));
  }
);

// @desc    Check subscription
// @route   POST /api/v1/subscriptions/check
// @access  Private
exports.checkSubscription = asyncHandler(
  async (req: UserRequest, res: Response, next: NextFunction) => {
    const channel = await Subscription.findOne({
      channelId: req.body.channelId,
      subscriberId: req.user._id,
    });

    if (!channel) {
      return res
        .status(200)
        .json(response(true, "You are not have channel", {}));
    }

    return res.status(200).json(response(true, "OK", channel));
  }
);

// @desc    Create subscriber
// @route   Post /api/v1/subscriptions
// @access  Private
exports.subscribe = asyncHandler(
  async (req: UserRequest, res: Response, next: NextFunction) => {
    const { channelId } = req.body;

    if (channelId.toString() == req.user._id.toString()) {
      return res
        .status(400)
        .json(response(false, `You can't subscribe to your own channel`, {}));
    }

    let subscription = await Subscription.findOne({
      channelId: channelId,
      subscriberId: req.user._id,
    });

    if (subscription) {
      await subscription.remove();
      return res.status(200).json(response(true, "remove", {}));
    } else {
      subscription = await Subscription.create({
        subscriberId: req.user._id,
        channelId: channelId,
      });
    }

    res.status(200).json(response(true, "create", subscription));
  }
);

exports.test = asyncHandler(
  async (req: UserRequest, res: Response, next: NextFunction) => {
    const sub = await Subscription.find({
      subscriberId: "5ff9384630892535cc39c041",
    }).populate("videos");
    res.status(200).json(sub);
  }
);
