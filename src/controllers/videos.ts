import { NextFunction, Request, Response } from "express";
import { MAX_FILE_UPLOAD } from "../config/database/config_env";
import { UserRequest } from "../config/request/user.request";

const { response } = require("../shared/utils/responseApi");

const path = require("path");
const fs = require("fs");
const asyncHandler = require("../shared/middleware/async");

const Video = require("../models/Video");
const History = require("../models/History");
// @desc    Get videos
// @route   GET /api/v1/videos/public or /api/v1/videos/private
// @access  Public Or Private
exports.getVideos = asyncHandler(
  async (req: Request, res: any, next: NextFunction) => {
    res.status(200).json(res.advancedResults);
  }
);
// @desc Get video[0]
// @route GET /api/v1/videos/new_video
// @access Private
exports.getLatestVideo = asyncHandler(
  async (req: UserRequest, res: any, next: NextFunction) => {
    const video = await Video.findOne(
      { userId: req.user._id },
      {},
      { sort: { createdAt: -1 } }
    )
      .populate({ path: "userId", select: "channelName subscribers photoUrl" })
      .populate("categoryId")
      .populate("likes")
      .populate("dislikes")
      .populate("comments");

    return res.status(200).json(response(true, "OK", video));
  }
);
// @desc    Get single video
// @route   GET /api/v1/videos/:id
// @access  Public
exports.getVideo = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const video = await Video.findById(req.params.id)
      .populate({
        path: "categoryId",
      })
      .populate({ path: "userId", select: "channelName subscribers photoUrl" })
      .populate({ path: "likes" })
      .populate({ path: "dislikes" })
      .populate({ path: "comments" });
    if (!video) {
      return res
        .status(404)
        .json(response(false, `No video with that id of ${req.params.id}`, {}));
    }
    res.status(200).json(response(true, "OK", video));
  }
);

// @desc    Upload video
// @route   POST /api/v1/videos
// @access  Private
exports.videoUpload = asyncHandler(
  async (req: any, res: Response, next: NextFunction) => {
    const { id } = req.user;
    let videoModel = await Video.create({ userId: id });
    if (!req.files) {
      return res.status(404).json(response(false, `Please upload a video`, {}));
    }
    const video = req.files.video;

    if (!video.mimetype.startsWith("video")) {
      await videoModel.remove();
      return res.status(404).json(response(false, `Please upload a video`, {}));
    }
    console.log(video.size, MAX_FILE_UPLOAD * 5);
    if (video.size > MAX_FILE_UPLOAD * 5) {
      await videoModel.remove();
      return res
        .status(404)
        .json(
          response(
            false,
            `Please upload a video less than ${
              (MAX_FILE_UPLOAD * 5) / 1000 / 1000
            }mb`,
            {}
          )
        );
    }
    video.originalName = video.name.split(".")[0];
    video.name = `video-${videoModel._id}${path.parse(video.name).ext}`;

    video.mv(
      `${process.env.FILE_UPLOAD_PATH}/videos/${video.name}`,
      async (err: Error) => {
        if (err) {
          await videoModel.remove();
          console.error(err);
          return res
            .status(500)
            .json(response(false, `Problem with video upload`, {}));
        }

        videoModel = await Video.findByIdAndUpdate(
          videoModel._id,
          {
            url: video.name,
            title: video.originalName,
          },
          { new: true, runValidators: true }
        );
        res
          .status(200)
          .json(response(true, "Upload video success", videoModel));
      }
    );
  }
);
// @desc    Update video
// @route   PUT /api/v1/videos/:id
// @access  Private
exports.updateVideo = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const video = await Video.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });
    if (!video)
      return res
        .status(404)
        .json(response(false, `No video with that id of ${req.params.id}`, {}));

    res.status(200).json(response(true, "OK", video));
  }
);

// @desc    Update video views
// @route   PUT /api/v1/videos/:id/views
// @access  Public
exports.updateViews = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    let video = await Video.findById(req.params.id);

    if (!video)
      return res
        .status(404)
        .json(response(false, `No video with that id of ${req.params.id}`, {}));
    video.views++;

    await video.save();

    res.status(200).json(response(true, "OK", video));
  }
);

// @desc    Upload thumbnail
// @route   PUT /api/v1/videos/:id/thumbnail
// @access  Private
exports.uploadVideoThumbnail = asyncHandler(
  async (req: any, res: Response, next: NextFunction) => {
    const video = await Video.findById(req.params.id);
    if (!video)
      return res
        .status(404)
        .json(response(false, `No video with that id of ${req.params.id}`, {}));

    if (!req.files) {
      return res.status(404).json(response(false, `Please upload file`, {}));
    }

    const file = req.files.thumbnail;

    if (!file.mimetype.startsWith("image")) {
      return res
        .status(404)
        .json(response(false, `Please update an image file`, {}));
    }

    if (file.size > MAX_FILE_UPLOAD) {
      return res
        .status(404)
        .json(
          response(
            false,
            `Please upload an image less than ${
              MAX_FILE_UPLOAD / 1000 / 1000
            }mb`,
            {}
          )
        );
    }

    file.name = `thumbnail-${video._id}${path.parse(file.name).ext}`;

    file.mv(
      `${process.env.FILE_UPLOAD_PATH}/thumbnails/${file.name}`,
      async (err: Error) => {
        if (err) {
          console.error(err);
          return res
            .status(500)
            .json(response(false, `Problem with file upload`, {}));
        }

        const tempVideo = await Video.findByIdAndUpdate(req.params.id, {
          thumbnailUrl: file.name,
        });

        res.status(200).json(response(true, "OK", tempVideo));
      }
    );
  }
);

// @desc    Delete video
// @route   DELETE /api/v1/videos/:id
// @access  Private
exports.deleteVideo = asyncHandler(
  async (req: UserRequest, res: Response, next: NextFunction) => {
    let video = await Video.findOne({
      userId: req.user._id,
      _id: req.params.id,
    });

    if (!video) {
      return res
        .status(404)
        .json(response(false, `No video with that id of ${req.params.id}`, {}));
    }
    await History.deleteOne({ videoId: req.params.id });
    await video.remove();
    console.log("video::", video);
    res.status(200).json(response(true, "OK", video));
  }
);
