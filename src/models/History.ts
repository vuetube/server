import mongoose from "mongoose";
import { SchemaEnum } from "../shared/enum/schema.enum";

const Schema = mongoose.Schema;
const historySchema = new Schema(
  {
    searchText: {
      type: String,
    },
    type: {
      type: String,
      enum: SchemaEnum.SEARCH,
      required: [true, "Type is required"],
    },
    userId: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: [true, "User id is required"],
    },
    videoId: {
      type: Schema.Types.ObjectId,
      ref: "Video",
    },
  },
  { timestamps: true }
);
module.exports = mongoose.model("History", historySchema);
