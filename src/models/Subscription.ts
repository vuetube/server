import mongoose from "mongoose";

const Schema = mongoose.Schema;
const subscriptionSchema = new Schema(
  {
    subscriberId: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: [true, "Subscriber id is required"],
    }, // yourself
    channelId: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    }, // themself
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    timestamps: true,
  }
);
subscriptionSchema.virtual("videos", {
  ref: "Video",
  localField: "channelId",
  foreignField: "userId",
  justOne: false,
  count: false,
  match: { status: "public" },
});
subscriptionSchema.virtual("users", {
  ref: "User",
  localField: "channelId",
  foreignField: "userId",
  justOne: false,
  count: false,
});
module.exports = mongoose.model("Subscription", subscriptionSchema);
