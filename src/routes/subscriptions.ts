const {
  getChannels,
  getSubscribers,
  subscribe,
  checkSubscription,
  test,
} = require("../controllers/subscriptions");
import express from "express";

const Subscription = require("../models/Subscription");

const routerSubscrip = express.Router();

const advancedResultsSubscrip = require("../shared/middleware/advancedResults");
const { protect } = require("../shared/middleware/auth");

routerSubscrip.post("/", protect, subscribe);

routerSubscrip.post("/check", protect, checkSubscription);

routerSubscrip.route("/subscribers").get(
  protect,
  advancedResultsSubscrip(
    Subscription,
    [{ path: "channelId" }, { path: "videos" }],
    {
      status: "private",
      filter: "subscribers",
    }
  ),
  getSubscribers
);

routerSubscrip.get("/channels/:channelId", protect, getChannels);
module.exports = routerSubscrip;
