/**
 * @desc    Send any response
 *
 * @param   {string} message
 * @param   {object | array} results
 */
exports.response = (success: boolean, message: string, results: object) => {
  return {
    success,
    message,
    results,
  };
};
/**
 * @desc    Send any advanced response
 *
 * @param   {boolean} success
 * @param   {string} message
 * @param   {object | array} results
 * @param   {number} count
 * @param   {number} totalPage
 * @param   {previous: {int } , next : {int }} pagination
 *
 */

exports.advancedResponse = (
  success: boolean,
  message: string,
  results: object,
  count?: number,
  totalPage?: number,
  pagination?: object
) => {
  return {
    success,
    message,
    results,
    count,
    totalPage,
    pagination,
  };
};
